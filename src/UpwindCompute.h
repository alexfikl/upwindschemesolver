#ifndef UPWINDCOMPUTE_H_
#define UPWINDCOMPUTE_H_

/**
 * @brief Compute the velocity on an edge.
 *
 * The velocity on an edge is computed based on the triangles that contain
 * it,  more exactly:
 *  - if two triangles contain it,  it is the mean of the velocities in both
 * triangles
 *  - if only one triangle contains it (boundary edge), it is the velocity
 * in that triangle.
 *
 * @param pb The problem struct.
 */
void MyComputeEdgeVelocity(problem_t* pb);

/**
 * @brief Compute the flux through an edge.
 *
 * We suppose we have an edge @c e and two cells @c cw and and @c ce. Since
 * this is the upwind scheme, the flux is the upstream value, this is decided
 * by looking at (u_e, n_e), the scalar product between the edge velocity
 * and the edge normal.
 *
 * The edge normal is always exterior to @c cw (the western cell), so this
 * means that the flux is:
 *  - the value in @c cw, if (u_e, n_e) > 0
 *  - the value in @c ce, if (u_e, n_e) <= 0
 *
 * @param pb The problem struct.
 */
void MyComputeFlux(problem_t* pb);

/**
 * @brief Compute the time step.
 *
 * The time step is computed in such a way that:
 *      -dt * max[ \sum_{e \in O} l_e * (u_e, n_e) ] <= CFL
 * where O is the set of edges between the current cell c and a neighbouring
 * cell c', such that c is the downwind neighbour of c'. The value of
 * the CFL constant is given by the user in `config.cfg`.
 *
 * @param pb The problem struct.
 */
void MyComputeTimeStep(problem_t* pb);

/**
 * @brief Compute the values in each cell at time t + dt.
 *
 * The values are updated using the formula:
 *  c^{n + 1}_j = c^n_j + \sum_{e} l_e (u_e, n_e) (c^n_e - f_e)
 * where c^n_e is the neighbour corresponding to the edge e and f_e is the
 * flux through the edge e.
 *
 * @param pb The problem struct.
 */
void MyUpdateCellValues(problem_t* pb);

#endif // UPWINDCOMPUTE_H_
