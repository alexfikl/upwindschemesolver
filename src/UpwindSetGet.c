#include <YAFiVoC.h>

#include "UpwindSolver.h"
#include "UpwindSetGet.h"

enum {
    xAxis = 0,
    yAxis = 1,
};

vect_t Solver_GetEdgeVelocity(solverData_t* data, int edgeId)
{
    vect_t res = {
        .x = GetVal(data->u, edgeId, xAxis),
        .y = GetVal(data->u, edgeId, yAxis)
    };

    return res;
}

real_t Solver_GetFlux(solverData_t* data, int edgeId)
{
    return GetVal(data->flux, edgeId, 0);
}

real_t Solver_GetCellValue(problem_t* pb, cell_t* cell)
{
    varArray_t* data = GetPbData(pb);
    int cellId = GetCellDataId(cell);

    return VarArrayGetRho(data, cellId);
}

int Solver_GetGlobalEdgeId(cell_t* T, int cellEdgeId)
{
    edge_t* edge = GetCellEdge(T, cellEdgeId);
    return GetEdgeId(edge);
}

int Solver_GetCellEdgeId(cell_t* cell, edge_t* edge)
{
    int cellEdgeId = -1;

    for (EachCellEdgeId(cell, cellEdgeId)) {
        if (GetCellEdge(cell, cellEdgeId) == edge) {
            return cellEdgeId;
        }
    }

    return -1;
}

void Solver_SetEdgeVelocity(solverData_t* data, int edgeId, vect_t value)
{
    SetVal(data->u, edgeId, xAxis, value.x);
    SetVal(data->u, edgeId, yAxis, value.y);
}

void Solver_SetFlux(solverData_t* data, int edgeId, real_t value)
{
    SetVal(data->flux, edgeId, 0, value);
}

void Solver_SetCellValue(problem_t* pb, cell_t* cell, real_t value)
{
    varArray_t* data = GetPbData(pb);

    int cellId = GetCellDataId(cell);
    vect_t cellVelocity = VarArrayGetVelocity(data, cellId);

    VarArraySetRho(data, cellId, value);

    /* TODO: remove when we find a better way to store our variable */
    VarArraySetRhoUx(data, cellId, value * cellVelocity.x);
    VarArraySetRhoUy(data, cellId, value * cellVelocity.y);
}
