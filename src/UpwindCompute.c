#include <YAFiVoC.h>

#include "UpwindCompute.h"
#include "UpwindSolver.h"
#include "UpwindSetGet.h"

/**
 * @brief Compute the coefficient for a term in the sum.
 *
 * The coefficient in front of a term in the sum is:
 *      l_e (u_e, n_e)
 *
 * This function computes that coefficient for an edge with the normal
 * exterior to a given cell.
 *
 * @param edge The edge.
 * @param cell The cell.
 *
 * @returns The value of the coefficient.
 *
 * @sa MyUpdateCellValues
 */
static real_t ComputeEdgeCoefficient(problem_t* pb, edge_t* edge, cell_t* cell)
{
    int cellEdgeId = Solver_GetCellEdgeId(cell, edge);
    int edgeId = GetEdgeId(edge);

    vect_t ue = Solver_GetEdgeVelocity(pb->solverData, edgeId);
    vect_t ne = GetEdgeNormal(edge);
    real_t le = GetEdgeLength(edge);

    int theta = GetCellNormalOrientationFromEdgeId(cell, cellEdgeId);

    return theta * le * (ue.x * ne.x + ue.y * ne.y);
}

/**
 * @brief Compute the edge velocity of an inner edge.
 *
 * This differs from the velocity of a boundary edge because an inner edge
 * is part of two cells.
 *
 * @param pb The problem struct.
 * @param edge The edge to compute the velocity for.
 *
 * @sa MyComputeEdgeVelocity
 */
static void ComputeInnerEdgeVelocity(problem_t* pb, edge_t* edge)
{
    varArray_t* data = GetPbData(pb);

    cell_t* cellE = GetEdgeCellE(edge);
    cell_t* cellW = GetEdgeCellW(edge);

    int idCellE = GetCellDataId(cellE);
    int idCellW = GetCellDataId(cellW);

    vect_t velocityE = VarArrayGetVelocity(data, idCellE);
    vect_t velocityW = VarArrayGetVelocity(data, idCellW);

    /* compute the velocity on the edge: mean between the two cells */
    int edgeId = GetEdgeId(edge);
    vect_t edgeVelocity = {
        .x = (velocityE.x + velocityW.x) / 2.0,
        .y = (velocityE.y + velocityW.y) / 2.0
    };

    Solver_SetEdgeVelocity(pb->solverData, edgeId, edgeVelocity);
}

/**
 * @brief Compute the velocity on a boundary edge.
 *
 * @param pb The problem struct.
 * @param edge The edge.
 *
 * @sa MyComputeEdgeVelocity
 */
static void ComputeOuterEdgeVelocity(problem_t* pb, edge_t* edge)
{
    varArray_t* data = GetPbData(pb);

    cell_t* cell = GetEdgeInnerCell(edge);
    int cellId = GetCellDataId(cell);
    vect_t cellVelocity = VarArrayGetVelocity(data, cellId);

    /* Neumann boundary conditions => cellE == cellW */
    int edgeId = GetEdgeId(edge);
    vect_t edgeVelocity = {
        .x = cellVelocity.x,
        .y = cellVelocity.y
    };

    Solver_SetEdgeVelocity(pb->solverData, edgeId, edgeVelocity);
}

/**
 * @brief Compute the flux through an inner edge.
 *
 * In this case we need to find the upwind value, and set it to that.
 *
 * @param pb The problem struct.
 * @param edge The edge
 *
 * @sa MyComputeFlux
 */
static void ComputeInnerFlux(problem_t* pb, edge_t* edge)
{
    int edgeId = GetEdgeId(edge);

    cell_t* cellW = GetEdgeCellW(edge);
    cell_t* cellE = GetEdgeCellE(edge);

    /* see whether the west cell is upwind */
    int isWUpwind = (ComputeEdgeCoefficient(pb,  edge,  cellW) > 0 ? 1 : 0);
    real_t flux = (isWUpwind ? Solver_GetCellValue(pb, cellW) :
                               Solver_GetCellValue(pb, cellE));

    Solver_SetFlux(pb->solverData, edgeId, flux);
}

/**
 * @brief Compute the flux through a boundary edge.
 *
 * This is just the value of the inner cell.
 *
 * @param pb The problem struct.
 * @param edge The edge.
 *
 * @sa MyComputeFlux
 */
static void ComputeOuterFlux(problem_t* pb, edge_t* edge)
{
    int edgeId = GetEdgeId(edge);
    cell_t* cell = GetEdgeInnerCell(edge);

    /* The value in the exterior cell is the same, so the flux is the same */
    real_t flux = Solver_GetCellValue(pb, cell);
    Solver_SetFlux(pb->solverData,  edgeId, flux);
}

void MyComputeTimeStep(problem_t* pb)
{
    real_t dt = 0.0;
    real_t dtmin = 2.0;

    mesh_t* mesh = GetPbMesh(pb);
    cell_t* cell = NULL;
    edge_t* edge = NULL;

    int cellEdgeId = -1;
    real_t cellArea = 0.0;
    real_t sum = 0.0;
    real_t edgeCoefficient = 0.0;

    for (EachMeshCell(mesh, cell)) {
        cellArea = GetCellArea(cell);
        sum = 0.0;

        for (EachCellEdgeId(cell, cellEdgeId)) {
            edge = GetCellEdge(cell, cellEdgeId);
            edgeCoefficient = ComputeEdgeCoefficient(pb, edge, cell);

            sum += (edgeCoefficient < 0 ? edgeCoefficient : 0);
        }

        dt = -cellArea / sum;
        dtmin = MIN(dt, dtmin);
    }

    dtmin = dtmin * pb->solverData->cfl;
    SetPbTimeStep(pb, dtmin);
}

void MyComputeEdgeVelocity(problem_t* pb)
{
    mesh_t* mesh = GetPbMesh(pb);
    edge_t* edge = NULL;

    for (EachMeshInnerEdge(mesh, edge)) {
        ComputeInnerEdgeVelocity(pb, edge);
    }

    for (EachMeshOutterEdge(mesh, edge)) {
        ComputeOuterEdgeVelocity(pb, edge);
    }
}

void MyComputeFlux(problem_t* pb)
{
    mesh_t* mesh = GetPbMesh(pb);
    edge_t* edge = NULL;

    for (EachMeshInnerEdge(mesh, edge)) {
        ComputeInnerFlux(pb, edge);
    }

    for (EachMeshOutterEdge(mesh, edge)) {
        ComputeOuterFlux(pb, edge);
    }
}

void MyUpdateCellValues(problem_t* pb)
{
    double dt = GetPbTimeStep(pb);
    mesh_t* mesh = GetPbMesh(pb);

    cell_t* cell = NULL;
    real_t cellValue = 0.0;
    real_t cellArea = 0.0;
    int cellId = -1;
    int cellEdgeId = -1;

    edge_t* edge = NULL;
    int edgeId = -1;
    real_t edgeCoefficient = 0.0;
    real_t flux = 0.0;

    real_t* delta = NULL;

    delta = SMalloc(GetMeshNbCells(mesh) * sizeof(real_t));

    for (EachMeshCell(mesh, cell)) {
        cellId = GetCellDataId(cell);
        cellArea = GetCellArea(cell);
        cellValue = Solver_GetCellValue(pb, cell);
        delta[cellId] = 0.0;

        for (EachCellEdgeId(cell, cellEdgeId)) {
            edge = GetCellEdge(cell, cellEdgeId);
            edgeId = GetEdgeId(edge);
            edgeCoefficient = ComputeEdgeCoefficient(pb, edge,  cell);
            flux = Solver_GetFlux(pb->solverData,  edgeId);

            delta[cellId] += (dt / cellArea) *
                             edgeCoefficient *
                             (cellValue - flux);
        }
    }

    for(EachMeshCell(mesh, cell)) {
        cellId = GetCellDataId(cell);
        cellValue = Solver_GetCellValue(pb, cell);

        cellValue = cellValue + delta[cellId];
        Solver_SetCellValue(pb, cell, cellValue);
    }

    free(delta);
}
