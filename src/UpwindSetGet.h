#ifndef UPWINDSETGET_H_
#define UPWINDSETGET_H_

/**
 * @brief Get the velocity of an edge.
 *
 * @param data The solver data struct.
 * @param edgeId The ID of the edge.
 *
 * @returns The velocity on the edge with the given ID.
 */
vect_t Solver_GetEdgeVelocity(solverData_t* data, int edgeId);

/**
 * @brief Get the flux on an edge.
 *
 * @param data The solver data struct.
 * @param edgeId The ID of the edge.
 *
 * @returns The flux on the edge with the given ID.
 */
real_t Solver_GetFlux(solverData_t* data, int edgeId);

/**
 * @brief Get the value in a cell.
 *
 * @param pb The problem struct.
 * @param cell The cell.
 *
 * @returns The value in the respective cell.
 */
real_t Solver_GetCellValue(problem_t* pb, cell_t* cell);

/**
 * @brief Get the global edge ID from the ID of the edge in a cell.
 *
 * @param cell The cell containing the edge.
 * @param cellEdgeId The ID of the edge inside the cell.
 *
 * @returns The global ID of the edge.
 */
int Solver_GetGlobalEdgeId(cell_t* cell, int cellEdgeId);

/**
 * @brief Get the ID of an edge inside a cell.
 *
 * @param cell The cell containing the edge.
 * @param edge The edge whose ID we want to find.
 *
 * @returns The ID of the edge inside the cell, -1 if the edge is not in the cell.
 */
int Solver_GetCellEdgeId(cell_t* cell,  edge_t* edge);

/**
 * @brief Set the value of the velocity on an edge.
 *
 * @param data The solver data struct.
 * @param edgeId The ID of the edge on which to set the velocity
 * @param value The new value of the velocity.
 */
void Solver_SetEdgeVelocity(solverData_t* data, int edgeId, vect_t value);

/**
 * @brief Set the value of the flux on an edge.
 *
 * @param data The solver data struct.
 * @param edgeId The ID of the edge.
 * @param value The new value of the flux.
 */
void Solver_SetFlux(solverData_t* data, int edgeId, real_t value);

/**
 * @brief Set the value in a cell.
 *
 * Note: This also updates other variables in the code: the product of
 * our variable and the velocity vector.
 *
 * @param pb The problem struct.
 * @param cell The cell to set it in.
 * @param value The new value in the cell.
 */
void Solver_SetCellValue(problem_t* pb, cell_t* cell, real_t value);

#endif // UPWINDSETGET_H_
