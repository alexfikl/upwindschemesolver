#include <YAFiVoC.h>

#include "UpwindSolver.h"
#include "UpwindCompute.h"

static void IncrementIteration(problem_t* pb)
{
    pb->solverData->iteration += 1;
}

void MyComputeSolverCFL(problem_t* pb)
{
    UNUSED(pb);
}

void MySolver(problem_t* pb)
{
    // compute the edge velocity
    MyComputeEdgeVelocity(pb);

    // compute the time step that satisfies the CFL condition
    MyComputeTimeStep(pb);

    // compute the flux on each edge
    MyComputeFlux(pb);

    // update the values in each cell
    MyUpdateCellValues(pb);

    // ta da! finished!
    IncrementTimeStep(pb);
    IncrementIteration(pb);
//     SetPbTime(pb, 2); // stop!
}

void MyInitializeSolver(problem_t* pb)
{
    solverData_t* solverData = NULL;
    mesh_t* mesh = GetPbMesh(pb);
    int nbEdges = GetMeshNbEdges(mesh);

    /* allocate memory for our solverData in the global problem struct */
    solverData = SMalloc(sizeof(solverData_t));
    pb->solverData = solverData;

    solverData->u = NewVarArray(nbEdges, 2);
    solverData->flux = NewVarArray(nbEdges, 1);
    solverData->iteration = 0;
    solverData->cfl = Config_ReadReal(pb->cfg, "solver.CFL");
}

void MyPrintSolverInfo(FILE* stream, problem_t* pb)
{
    fprintf(stream, "Solver: UPWIND\n");
    fprintf(stream, "n  = %d\n", pb->solverData->iteration);
    fprintf(stream, "t  = %g\n", GetPbTime(pb));
    fprintf(stream, "dt = %g\n", GetPbTimeStep(pb));
}

real_t MyGetSolverTimeStep(problem_t* pb)
{
    return GetPbTimeStep(pb);
}

void MyFinalizeSolver(problem_t* pb)
{
    FreeVarArray(pb->solverData->flux);
    FreeVarArray(pb->solverData->u);

    free(pb->solverData);
}
