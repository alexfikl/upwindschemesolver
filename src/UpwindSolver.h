#ifndef UPWINDSOLVER_H_
#define UPWINDSOLVER_H_

struct solverData_t {
    varArray_t* u;
    varArray_t* flux;

    real_t cfl;
    int iteration;
};

/**
 * @brief Compute the CFL condition for our problem.
 *
 * UNUSED
 *
 * @param[in,out] pb The problem.
 */
void MyComputeSolverCFL(problem_t* pb);

/**
 * @brief Solve one time step for our problem.
 *
 * @param[in,out] pb The problem.
 */
void MySolver(problem_t* pb);

/**
 * @brief Initialize the solverData @c struct.
 *
 * @param[in,out] pb The problem.
 */
void MyInitializeSolver(problem_t* pb);

/**
 * @brief Print some information about the current state of the problem
 *
 * @param[in] stream File stream to write to.
 * @param[in] pb The problem.
 */
void MyPrintSolverInfo(FILE* stream, problem_t* pb);

/**
 * @brief Get the next time step for our problem.
 *
 * @param[in,out] pb The problem.
 *
 * @return The global problem time step.
 */
real_t MyGetSolverTimeStep(problem_t* pb);

/**
 * @brief Free the solverData struct.
 *
 * @param[in,out] pb The problem.
 */
void MyFinalizeSolver(problem_t* pb);

#endif // UPWINDSOLVER_H_
