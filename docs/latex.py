#!/usr/bin/python

import sys
from docutils.core import publish_cmdline

settings = """
% special settings
\setlength{\parindent}{0em}
"""

options = [
    "--documentclass=scrbook",
    "--documentoptions=DIV14,10pt,parskip=half*",
    "--latex-preamble=" + settings,
    "--hyperlink-color=black",
    "--use-verbatim-when-possible",
    sys.argv[1]
    ]

publish_cmdline(writer_name="latex",
                argv=options,
                description="Modified LaTeX Convertor. Also see rst2latex.")
