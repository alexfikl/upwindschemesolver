#!/bin/sh

TEST=${1:-test01}
DIR=../../install/
EXE=${DIR}/bin/YAFiVoC.exe

source ${DIR}/env.sh

${EXE} ${TEST}
